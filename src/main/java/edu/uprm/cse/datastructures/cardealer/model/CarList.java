package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

//class designed to hold the list with the car objects
public class CarList {
	//a private instance of the list is created that cannot be changed
	private static final CircularSortedDoublyLinkedList<Car> clist = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	//method to get the list 
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return clist;
	}
	//method to clean the list
	public static void resetCars(){
		clist.clear();;
	}
}
