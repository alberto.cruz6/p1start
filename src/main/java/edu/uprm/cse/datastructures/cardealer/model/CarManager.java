package edu.uprm.cse.datastructures.cardealer.model;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

//Access to methods of list
@Path("/cars")
public class CarManager {
	private final CircularSortedDoublyLinkedList<Car> clist = CarList.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//this method will gather all car elements into an array and display them
	public Car[] getAllCars() {
		Car[] allCars = new Car[clist.size()];
		for (int i = 0; i < clist.size(); i++) {//Iterates over list and adds elements into array
			allCars[i] = clist.get(i);
		}
		return allCars;
	} 
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	//this method will iterate through all car until it finds the one with the same id
	public Car getCar(@PathParam("id") long id){
		for(Car target:clist) {//iterates over carlist until it finds the target car
			if(target.getCarId() == id) {
				return target;
			}
		}
		//if not found throw an error
		throw new NotFoundException(new JsonError("Error" , "Car " + id + " not found"));
	}       
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	//this method will utilize the list add method to append a new car element
	//it will not add an already existing id 
	public Response addCar(Car obj){
		for(Car target:clist) {//if car exists in list it will not be added
			if(target.getCarId() == obj.getCarId()) {
				return Response.status(Response.Status.CONFLICT).build(); 
			}
		}
		clist.add(obj);//add car into the list
		return Response.status(201).build();
	}
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	//this method will delete the car with the id of the parameter car and change it for the car object given 
	public Response updateCar(Car obj){
		for (Car car : clist) {//find car and remove it to add the new object in it's place (id)
			if(car.getCarId() == obj.getCarId()){
				clist.remove(car);
				clist.add(obj);
				return Response.status(Response.Status.OK).build();
			}
		}

		//if not found return that it's not found
		return Response.status(Response.Status.NOT_FOUND).build(); 
	}     
	@DELETE
	@Path("/{id}/delete")
	//this method will utilize the  car remove method to delete the element with the specified id
	public Response deleteCar(@PathParam("id") long id){
		for (Car car : clist) {
			if(car.getCarId() == id){
				clist.remove(car);//find car in list and remove
				return Response.status(Response.Status.OK).build();
			}
		} 
		//if it doesn't exist then it will return a not found error
		return Response.status(Response.Status.NOT_FOUND).build(); 

	}
}
