package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	@Override
	public Iterator<E> iterator(int index) {		
		return new CircularSortedDoublyLinkedListIterator<E>(index);
	}

	@Override
	public Iterator<E> iterator() {		
		return new CircularSortedDoublyLinkedListIterator<E>();
	}
	@Override
	public ReverseIterator<E> reverseiterator() {		
		return new CircularSortedDoublyLinkedListReverseIterator<E>();
	}
	@Override
	public ReverseIterator<E> reverseiterator(int index) {		
		return new CircularSortedDoublyLinkedListReverseIterator<E>(index);
	}

	private class  CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;


		public  CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();

		}
		public  CircularSortedDoublyLinkedListIterator(int index) {
			int count = 0;
			this.nextNode = (Node<E>) header;
			while(count <= index) {
				this.nextNode = this.nextNode.getNext();
				count++;
			}	
			

		}
		@Override
		public boolean hasNext() {
			return nextNode.element != null;
		}

		
		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}


	}
	private class  CircularSortedDoublyLinkedListReverseIterator<E> implements ReverseIterator<E>{
		private Node<E> previousNode;
		

		public  CircularSortedDoublyLinkedListReverseIterator() {
			this.previousNode = (Node<E>) header.getPrevious();	
		}
		
		public  CircularSortedDoublyLinkedListReverseIterator(int index) {			
			int count = 0;
			this.previousNode = (Node<E>) header;
			while(count <= index) {
				this.previousNode = this.previousNode.getNext();
				count++;
			}
		}
		
		@Override
		public boolean hasPrevious() {
			return previousNode.element != null;
		}

		
		@Override
		public E previous() {
			if (this.hasPrevious()) {
				E result = this.previousNode.getElement();
				this.previousNode = this.previousNode.getPrevious();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}


	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}
		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}


	}
	//Linked List values with a custom comparator
	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;


	public  CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		//constructor receives a comparator
		this.comparator = cmp;
		//creates dumm yheader
		this.header = new Node<>();
		//initial size
		this.currentSize = 0;
		this.comparator = cmp;
		//header will be connected to itself 
		this.header.setNext(this.header);
		this.header.setPrevious(this.header);
	}
	@Override
	public boolean add(E obj) {
		//check if element is valid
		if(obj == null) {
			return false;
		}
		//create the node with the element
		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);
		//if list has no elements yet, add in the first spot
		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			newNode.setNext(this.header);
			newNode.setPrevious(this.header);
			this.currentSize++;
			return true;
		}
		//if not empty, proceeds to compae with all the other elements
		//node that will iterate over the list
		Node<E> temp = this.header.getNext();
		newNode.setElement(obj);
		while(true) {
			//if elemnt in front is the same one add right in front of it
			if(this.comparator.compare(temp.getElement(), newNode.getElement()) == 0) {
				newNode.setPrevious(temp.getPrevious());
				temp.getPrevious().setNext(newNode);
				temp.setPrevious(newNode);
				newNode.setNext(temp);
				this.currentSize++;
				return true;


			}
			//if it's between a node that is smaller and the header add in between
			else if(comparator.compare(temp.getElement(), newNode.getElement()) <0 &&
					temp.getNext().getElement() == null) {
				
				newNode.setNext(header);
				this.header.setPrevious(newNode);
				temp.setNext(newNode);
				newNode.setPrevious(temp);
				this.currentSize++;
				return true;
			}
			//if it's between the header and a node that is bigger add in between
			else if(temp.getPrevious().getElement() == null && comparator.compare(temp.getElement(), newNode.getElement())>0) {

				newNode.setNext(temp);
				temp.setPrevious(newNode);
				header.setNext(newNode);
				newNode.setPrevious(header);
				this.currentSize++;
				return true;
			}
			//if it's between a node that's smaller and a node that it's bigger add in between
			else if(comparator.compare(temp.getElement(), newNode.getElement()) < 0  &&
					comparator.compare(temp.getNext().getElement(), newNode.getElement()) >0) {

				newNode.setNext(temp.getNext());
				temp.getNext().setPrevious(newNode);
				temp.setNext(newNode);
				newNode.setPrevious(temp);
				this.currentSize++;
				return true;
			}
			//if it reaches header again add  behind it
			else if(temp.getNext().getElement() == null) {
				temp.setNext(newNode);
				newNode.setPrevious(temp);
				newNode.setNext(this.header);
				this.header.setPrevious(newNode);
				this.currentSize++;
				return true;
			}



			temp = temp.getNext();

		}
		//return false;

	}

	@Override
	public int size() {

		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		//check if object is valid
		if(obj == null) {
			return false;
		}
		//node that will iterate
		Node<E> temp = this.header.getNext();

		while(true) { 
			//find the element and remove from the list
			if(this.comparator.compare(temp.getElement(), obj) == 0) {
				temp.getPrevious().setNext(temp.getNext());
				temp.getNext().setPrevious(temp.getPrevious());
				temp.setNext(null);
				temp.setPrevious(null);
				temp.setElement(null);
				this.currentSize--;
				return true; 
			}
			//if it's not on the list (reaches header without deleting) return false
			else if(temp.getNext().getElement() == null) {
				return false;
			}
			temp = temp.getNext();
		}


	} 

	@Override
	public boolean remove(int index) {
		//check that the index is valid
		if(index < 0 || this.size() <= index) {
			throw new IndexOutOfBoundsException();
		}
		//node that will iterate
		Node<E> temp = this.header.getNext();
		//reach the node that will be deleted
		while(index != 0) {
			temp = temp.getNext();
			index--;
		}
		//delete the node
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		temp.setNext(null);
		temp.setPrevious(null);
		temp.setElement(null);
		this.currentSize--;
		return true; 
	}

	@Override
	public int removeAll(E obj) { 
		int count = 0;
		//while able to remove, keep removing
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		//return the first element
		return this.header.getNext().getElement();
	}

	@Override 
	public E last() {
		//return the last element
		return this.header.getPrevious().getElement(); 
	}

	@Override
	public E get(int index) {
		//check if index is valid
		if(index < 0 || this.size() <= index) {
			throw new IndexOutOfBoundsException();
		}
		//node that will iterate
		Node<E> result = this.header;
		//iterate until the specified node
		for (int i = 0; i <= index; i++) {
			result = result.getNext();
		}

		return result.getElement();
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}

	@Override
	public boolean contains(E e) {

		if(this.firstIndex(e)!= -1) {
			return true;
		}

		return false;

	}

	@Override
	public boolean isEmpty() {
		if(this.size() == 0) {
			return true;
		}
		return false;
	}

	@Override
	public int firstIndex(E e) {

		Node<E> temp = this.header.getNext();
		int count = 0;
		//iterate until you find the node with the element
		while(true) {
			//if found return the index
			if(this.comparator.compare(temp.getElement(), e) == 0) {
				return count;
			}
			//else return -1
			else if(temp.getNext().getElement() == null) {
				return -1;
			} 
			count++;
			temp = temp.getNext();
		}


	}

	@Override
	public int lastIndex(E e) {
		Node<E> temp = this.header.getPrevious();
		int count = this.currentSize - 1;
		while(true) {
			//iterate until you find the node with the element
			//if found return the index
			if(this.comparator.compare(temp.getElement(), e) == 0) {
				return count;
			}
			//else return -1
			else if(temp.getPrevious().getElement() == null) {
				return -1;
			} 
			count--;
			temp = temp.getPrevious();
		}
	}

}