package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		//if elements are equal return 0
		if(o1.equals(o2)) {
			return 0;
		}
		//if brand is equal check that car model is equal
		if(o1.getCarBrand().compareTo(o2.getCarBrand())== 0) {
			//if model is equal check that car model option is equal
			if(o1.getCarModel().compareTo(o2.getCarModel()) ==0) {
				////compare car model option
				return o1.getCarModelOption().compareTo(o2.getCarModelOption());
			}
			//if car model not equal, compare them
			else {
				return o1.getCarModel().compareTo(o2.getCarModel());
			}
		}
		//if car brand not equal compare them
		return o1.getCarBrand().compareTo(o2.getCarBrand());

	}
}
